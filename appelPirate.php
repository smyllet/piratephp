<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 27/09/2019
 * Time: 10:13
 */
// Your custom class dir

require "autoload.php" ;

Marin::quiSuisJe();
$gentilMarin = new Marin("Robert", "prof") ;
echo "$gentilMarin" ;

echo "<br><br>";

$robert = new Capitaine("Robert", "prof", "senior") ;
$robert->commande() ;

echo "$robert" ;

echo "<br><br>";

$navire = new Navire([$gentilMarin,$robert],50,"Black Pearl");
$navire->addMarin(new Marin("Michel","Moussaillon"));

echo $navire;

$pirate = new Pirate("Bob", "Traitre", 50);
$pirate->pillage($navire);

echo "<br><br>";

echo $navire;

