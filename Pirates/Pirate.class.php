<?php

class Pirate extends Marin implements Piraterie
{
    private $sauvagerie;

    public function __construct($nom, $fonction, $sauvagerie)
    {
        parent::__construct($nom, $fonction);
        $this->sauvagerie = $sauvagerie;
    }

    public function pillage($navire)
    {
        if($this->sauvagerie>50)
        {
            $navire->setEquipage(array());
            $navire->setAttackPirate(true);
        }
        else
        {
            foreach ($navire->getEquipage() as $marin)
            {
                if($marin instanceof Capitaine)
                {
                    $navire->removeMarin($marin);
                }
            }
        }
    }
}