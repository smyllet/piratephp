<?php

class Navire
{
    private $equipage;
    private $taille;
    private $modele;
    private $attackPirate;

    public function __construct($equipage, $taille, $modele)
    {
        $this->equipage = $equipage;
        $this->taille = $taille;
        $this->modele = $modele;
        $this->attackPirate = false;
    }

    /**
     * @return mixed
     */
    public function getEquipage()
    {
        return $this->equipage;
    }

    /**
     * @param mixed $equipage
     */
    public function setEquipage($equipage)
    {
        $this->equipage = $equipage;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * @return mixed
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @param mixed $modele
     */
    public function setModele($modele)
    {
        $this->modele = $modele;
    }

    /**
     * @return false
     */
    public function getAttackPirate()
    {
        return $this->attackPirate;
    }

    /**
     * @param false $attackPirate
     */
    public function setAttackPirate($attackPirate)
    {
        $this->attackPirate = $attackPirate;
    }



    public function addMarin(Marin $marin)
    {
        $this->equipage[] = $marin;
    }

    public function removeMarin(Marin $marin)
    {
        $this->equipage = array_diff($this->equipage,[$marin]);
    }

    public function __toString()
    {
        $message = "Le navire mesure : ".$this->taille."<br>Son modèle est : ".$this->modele;
        if($this->getEquipage() == [])
        {
            if($this->getAttackPirate() == true)
            {
                $message.="<br>L'équipage à été victime d'un acte de piraterie";
            }
            else
            {
                $message.="<br>Ce navire ne dispose pas encore d'un équipage";
            }
        }
        else
        {
            $message.="<br>Liste de l'équipage : <br>";
            foreach ($this->equipage as $marin)
            {
                $message.=" - ".$marin."<br>";
            }
        }

        return $message;
    }
}